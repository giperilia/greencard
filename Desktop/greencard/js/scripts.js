$(document).ready(function () {

    $('.select-autotype__type').click(function () {
        $('.select-autotype__wrapper li').removeClass('autotype__type__selected');
        $(this).addClass('autotype__type__selected');
    })
    $("#date").datepicker();
    $(".phone").mask("+9 999 999 99 99");
    $("#phone").mask("+9 (999) 999 99 99")

    /* Calculator */

    function trackValueChanges() {
        let arrayOfValues = [
            $('.destination-value option:selected'),
            $('.time-value option:selected'),
            $('.auto-get-price__delivery-method input:checked')
        ]
        let defaultPrice = $('.autotype__type__selected').data('mod');
        let emergValue = $('#delivery-emergency-time:checked').data('mod') || 0
        let displayValue = 0;

        function getFinalPrice() {
            let finalPrice = arrayOfValues.reduce((start, cur) => {
                    return start *= cur.data('mod')
                }, defaultPrice
            )
            return displayValue = finalPrice + emergValue
        }

        getFinalPrice();
        $('.final-price-value').text(`${displayValue} руб.`);
    }

    $('.select-autotype__type').click(function () {
        trackValueChanges()
    })
    $('#form').change(function () {
        trackValueChanges()
    })
    if ($(window).width() > 768 && !$('.select-autotype__wrapper').hasClass('prices-ul-fix')) {
        $(".select-autotype__wrapper > li:first-child").addClass('autotype__type__selected');
        trackValueChanges() //Get initial price on page loading
    }

    /* Hamburger Animation */
    $(document).ready(function () {
        $('#nav-icon1').click(function () {
            $(this).toggleClass('open');
            $('.page-background').toggle();
            if ($(this).hasClass('open')) {
                $('.header__menu__menu-wrapper').css({'right':'0%'})
                $('html, body').css({'overflow':'hidden', 'height':'100%'})
            }
            else {
                $('.header__menu__menu-wrapper').css({'right':'-200%'})
                $('html, body').css({'overflow':'auto', 'height':'auto'})
            }
        });
    });

    /* Insert Type of car ~ mobile */
    $('.select-autotype__type').click(function (e) {
        let text = $(' .select-autotype__type__name', this).text();
        $('.mobile-autotype').text(text)

    })
    $(document).ready(function () {
        $('.mobile-autotype').text($('.autotype__type__selected').text());
    })


    /* Modal form on mobile device */
    $('.select-autotype__type').click(function () {
        if ($(window).width() <= 768) {
            $('#form').modal({backdrop: 'static'})
        }
    })

    /* Submenu animation on mobile */
    $('.has-children').click(function($event) {
        $($event.target).find('.header__submenu').slideToggle('fast');
    })

    /* Track form modal resizing */

    $(window).resize(function () {
        console.log
        if ($(document).width() <= 768) {
            if (!$('#form').hasClass('show')) {
                $("#form").css({"display": "none"})
            }
        }
        else {
            $("#form").css({"display": "inherit"})
        }
    })

    /* 404 scrolling*/
    if ($(document).find('.notfound').length) {
        $('html,body').animate({
            scrollTop:$('.notfound').offset().top
        },1000);
    }

    /* Scrollup button*/
    $(window).scroll(function () {
        if ($(window).scrollTop() > 900) {
            if (!$('.scrollup').hasClass('flex')) $('.scrollup').addClass('flex');
        }
        else $('.scrollup').removeClass('flex');
    })

    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });


    /* FAQ Get more implementation */
    $(function () {
        let faqElemArray = $('.faq__item__list');
        for (let i = 0; i < faqElemArray.length; i++) {
            $(faqElemArray[i]).find('li').slice(0,2).show();
        }
    })
    $(".faq-loadmore").on('click', function (e) {
        e.preventDefault();
        console.log(e);
        $(e.target).parent().find('.faq__item__list li:hidden').slice(0, 2).slideDown();
        console.log($(e.target).parent().find('.faq__item__list li:hidden').length)
        if ($(e.target).parent().find('.faq__item__list li:hidden').length == 0) {
            console.log()
            $(this).hide();
        }
    });

});

